import React, { useState, createContext } from "react";

export const LoginContext = createContext()
export const LoginProvider = props => {
    const menuListAll = [[
        { text: "Home", path: "/" },
        { text: 'About', path: "/about" },
        { text: 'Movie List Editor', path: "/movies-list" },
        { text: 'Login', path: "/login" }
      ]]
    const [login, setLogin] = useState({
        username: "",
        password: ""
    })
    const [isLogin, setIsLogin] = useState(0)
    const [menuList, setMenuList] = useState(menuListAll)

    return (
        <LoginContext.Provider value={[login, setLogin, isLogin, setIsLogin, menuListAll, menuList, setMenuList]}>
            {props.children}
        </LoginContext.Provider>
    )
}