import React, { useState, useContext } from "react";
import { LoginContext } from "./LoginContext";

const Login = () => {
    const daftarEmail = [
        {id: 1, username: 'hasan.choiri@gmail.com', password: '123'}
    ]
    const [login, setLogin, isLogin, setIsLogin] = useContext(LoginContext)

    const handleChange = (event) => {
        let paramsLogin = {...login}
        paramsLogin[event.target.name] = event.target.value
        setLogin(paramsLogin)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const found = daftarEmail.find(el => el.username == login.username && el.password == login.password)
        if (found) {
            setLogin({
                username: "",
                password: ""
            })
            setIsLogin(true)
        } else {
            window.alert('Username dan Password tidak terdaftar')
        }
    }

    return(
        <>
            <section>
                <form onSubmit={handleSubmit} className="demoForm">
                    <h2>Login</h2>
                    <table border={0} width={"100%"} style={{alignItems: "center"}}>
                        <tr>
                            <td>Username</td>
                            <td><input type="email" name="username" value={login.username} onChange={handleChange} placeholder="Username anda..."  required/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input name="password" value={login.password} onChange={handleChange} placeholder="Password anda..."  required/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button type="submit">Login</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <br/> <br/>
            </section>
        </>
    )
}

export default Login