import React, { useContext, useEffect, useState } from "react";
import { Switch, Link, Route } from "react-router-dom";

import "./public/css/style.css"
import Logo from "./public/img/logo.png"
import { MoviesProvider } from "../Movies/MoviesContext";
import { LoginContext } from "../Login/LoginContext";
import Home from "../Home/Home";
import About from "../About/About"
import Login from "../Login/Login";

const Routes = () => {
  const [login, setLogin, isLogin, setIsLogin, menuListAll, menuList, setMenuList] = useContext(LoginContext)

  return (
      <>
        <body>
          <header>
            <img id="logo" src={Logo} width="200px" />
            <nav>
              <ul>
                {menuList !== null && menuList.map((item, index)=>{
                  return(
                  <li><Link to={item.path}>{item.text}</Link></li>
                  )
                })}
              </ul>
            </nav>
          </header>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/movies-list" component={About} />
            <Route path="/about" component={About} />
            <Route path="/">
              <MoviesProvider>
                <Home />
              </MoviesProvider>
            </Route>
          </Switch>
          {/* <footer>
            <h5>Copyright &copy; 2020 by Sanbercode</h5>
          </footer> */}
        </body>
      </>
    )
}

export default Routes;