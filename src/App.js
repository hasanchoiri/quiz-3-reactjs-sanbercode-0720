import React from 'react';
import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"

import './App.css';
import Routes from './Routes/Routes';
import { LoginProvider } from './Login/LoginContext';

function App() {
  return (
    <div className="App">
      <LoginProvider>
      <Router>
        <Routes />
      </Router>
      </LoginProvider>
    </div>
  );
}

export default App;
