import React, { useState, createContext } from "react";

export const MoviesContext = createContext()
export const MoviesProvider = props => {
    const [movies, setMovies] = useState(null)
    const [input, setInput]  =  useState({name: "", price: 0, weight: 0})
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    return (
        <MoviesContext.Provider value={[movies, setMovies, input, setInput, selectedId, setSelectedId, statusForm, setStatusForm]}>
            {props.children}
        </MoviesContext.Provider>
    )
}