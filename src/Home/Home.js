import React, { useEffect, useContext } from 'react';
import { MoviesContext } from '../Movies/MoviesContext';
import axios from 'axios';

const Home = () => {
    const [movies, setMovies] = useContext(MoviesContext)

    useEffect(() => {
        if (movies === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                console.log(res)
                setMovies(res.data.map(el=>{
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating
                    }
                }))
            })
        }
    }, [movies])

    return (
        <>
            <section>
                <h1>Daftar Film Terbaik</h1>
                <div id="article-list">
                    {movies !== null && movies.map((item, index)=>{
                        return(
                            <div>
                                Title: <a href="">{item.title}</a>
                                <p>
                                    Rating: {item.rating}<br/>
                                    Durasi: {item.duration} jam<br/>
                                    Genre: {item.genre}<br/>
                                    Deskripsi: <br/>{item.description}
                                </p>
                            </div>
                        )
                    })}
                </div>
            </section>
        </>
    )
}

export default Home